#!/usr/bin/python3

# -*- coding: utf-8 -*-

import json
import sys

import tmdbsimple
from guessit import guessit


def search_movie(movie_folder_name):

    query_year = None
    query_title = movie_folder_name
    try:
        guess = guessit(movie_folder_name)
        if 'title' in guess:
            query_title = guess['title']
        if 'year' in guess:
            query_year = guess['year']
    except:
        pass

    tmdbsimple.API_KEY = '1e47bd048f9e83c93ef58d8897b5af54'
    search = tmdbsimple.Search()
    search.movie(query=query_title)
    search_result = search.results

    chosen = 0

    if query_year:
        try:
            closest_one = 0
            gap = 100
            for i, s in enumerate(search_result):
                year_str = s['release_date'][:4]
                if year_str:
                    year = int(year_str)
                    if query_year == year:
                        gap = 0
                        chosen = i
                        break
                    else:
                        if abs(query_year - year) < gap:
                            closest_one = i
                            gap = abs(query_year - year)
            if gap > 0:
                chosen = closest_one
        except:
            pass

    json_str = json.dumps(search_result[chosen])
    print('{}\t{}'.format(movie_folder_name, json_str))


if __name__ == '__main__':
    for query_name in sys.argv[1:]:
        try:
            search_movie(query_name)
        except:
            print('{}\t{}'.format(query_name, '{}'))
            pass
